import matplotlib.pyplot as plt
import numpy as np
import torch
from diffusers import DDPMPipeline
from torchvision import transforms

import dataset_diffusion


def get_event():
    from tensorboard.backend.event_processing import event_accumulator

    # Provide the path to your events file
    events_file_path = './outputmask/logs/train_example/events.out.tfevents.1702985548.Bumblebee.21960.0'

    # Create an EventAccumulator
    event_acc = event_accumulator.EventAccumulator(events_file_path)
    event_acc.Reload()

    # Get the available scalar tags
    scalar_tags = event_acc.Tags()['scalars']

    # Access scalar values for a specific tag (e.g., 'loss')
    loss_values = event_acc.Scalars('loss')

    # Extract epoch and loss values for plotting
    epochs = [event.step for event in loss_values]  # Assuming steps represent epochs
    losses = [event.value for event in loss_values]

    # Plot the loss values over epochs
    plt.plot(epochs, losses, label='Loss')
    plt.title('Loss Over Epochs')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()


def interference():
    print("interference maskimg")
    # Set the device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load the DDPMPipeline
    pipe = DDPMPipeline.from_pretrained("outputmask_diffmaskandimg").to(device)
    print(pipe)

    concatenated_tensor = pipe().images[0]
    img_array = np.array(concatenated_tensor)
    mask = img_array[ :, :,0]  # Extract the mask (first channel)
    image = img_array[ :, :,1:]  # Extract the image (remaining channels)

    # Visualize mask
    plt.figure(figsize=(6, 6))
    plt.imshow(mask)  # Assuming 0-2 are the class labels
    plt.title('Mask')
    plt.colorbar()
    plt.show()

    # Visualize image
    plt.figure(figsize=(6, 6))
    plt.imshow(image)
    plt.title('Image')
    plt.show()

def look():
    #input_size=(256,256)
    #input_size=(512,512)
    input_size=(128,128)

    trans = transforms.Compose([
        transforms.Resize(input_size),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])  # imagenet
    ])
    trans_label = transforms.Compose([
        transforms.Resize(input_size),
        transforms.ToTensor(),
    ])
    dataset = dataset_diffusion.CropSegmentationDataset("train", transform=trans, target_transform=trans_label)
    concatenated_tensor = dataset[2]
    # Assuming concatenated_tensor has shape [batch_size, 4, height, width]
    mask = concatenated_tensor[ 0, :, :]  # Extract the mask (first channel)
    image = concatenated_tensor[ 1:, :, :]  # Extract the image (remaining channels)

    # Unnormalize and convert to numpy array for visualization
    image_tensor = torch.clamp(image, 0, 1)
    image_np = (image_tensor.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
    label_np = mask.numpy()

    """Visualize image and label with original in 0,1,2"""
    # Plot the image and labels side by side with colorbars
    fig, axs = plt.subplots(1, 2, figsize=(12, 6))

    im0 = axs[0].imshow(image_np)
    axs[0].set_title('Original Image')
    cbar0 = plt.colorbar(im0, ax=axs[0], fraction=0.046, pad=0.04)

    im1 = axs[1].imshow(label_np, cmap='viridis', vmin=0, vmax=2)  # Assuming 0-4 are the class labels
    axs[1].set_title('Segmentation Label')
    cbar1 = plt.colorbar(im1, ax=axs[1], ticks=[0, 1, 2], fraction=0.046, pad=0.04)
    cbar1.set_ticklabels(['background', 'crop', 'weed'])

    plt.tight_layout()
    plt.show()
if __name__ == "__main__":
    # look()
    interference()
