# AdvancedImgProcessing

[Our report](./Report Project AMIP.pdf)

## Steps to get the project running

1. Clone repository
2. Install requirements.txt

## How to infere with segmentation model

1. Change `ROOT_PATH` in `dataset.py` (l. 13) to the location of your dataset 
2. *Optional*: Change weights of UNet in `train.py` (l. 402)
3. *Optional*: Change the resolution of the input images in `train.py` (l. 43)
4. *Optional*: Our output consists of three channels. One for background, crop and weed. If you want to get the original label shape with one channel and [0, 1, 2], uncomment l. 427 in `train.py`. 
5. *Optional*: You can use this `pred_squeezed` to calculate a score of our network.
6. Execute `main.py`

## How to use diffusion model
1. In the `Mask_generation` and the `Mask_Img_generation` folders are the according diffusion models.
2. Execute the `loadmaskimg.py` and the `load sample.py`.
3. *Optional*: In `load sample.py` you can change the model (l. 40: `pipe = DDPMPipeline.from_pretrained("outputmask256x256_normalized").to(device)`)

## The models

| Model name in report | Type         | Model name in project                             |
| -------------------- | ------------ | ------------------------------------------------- |
| Model 1              | Segmentation | model3_25_epoch_withweights.pth                   |
| Model 2              | Segmentation | Unet_model_fully_trained_sans_weights_25epoch.pth |
| Model 3              | Segmentation | Unet25epochmitFocalloss.pth                       |
| Model 4              | Segmentation | Unet_in_full_HD.pth                               |
| Model 5              | Diffusion    | outputmask256x256_normalized                      |
| Model 6              | Diffusion    | outputmask256x256withoutnorm                      |
| Model 7              | Diffusion    | outputmask_128x128_withoutnorm                    |
| Model 8              | Diffusion    | outputmask_diffmaskandimg                         |


