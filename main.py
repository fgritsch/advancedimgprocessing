import train
import model_unet
import torch

if __name__ == "__main__":
    if not torch.cuda.is_available():
        print("you need to install cuda")
    else:
        #train.run(model_unet.UNet,25,8)
        train.interference(model_unet.UNet)
