import copy
import time
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
from torchvision import transforms
from tqdm import tqdm

import dataset


def weight_class(target, nsamples=(512 * 512)):
    """
    Claculates the weight of the classes
    :param target: batch of labels
    :param nsamples: how many pixles are in the image
    :return: weights
    """
    num_classes = 3
    weights = torch.zeros(3)
    num_zeros = torch.sum(target == 0).item()
    num_ones = torch.sum(target == 1).item()
    num_twos = torch.sum(target == 2).item()
    weights[0] = nsamples / ((num_classes * num_zeros) + 1)
    weights[1] = nsamples / ((num_classes * num_ones) + 1)
    weights[2] = nsamples / ((num_classes * num_twos) + 1)
    return weights
def get_data_loaders(batchsize,weights=False):
    """
    Gets dataloaders and loads dataset
    :param batchsize: ...
    :param weights: Set to true if you want to have them in loss
    :return: dataloaders
    """
    # use the same transformations for train/val in this example
    "Set to 1024 when working with FullHD Net"
    input_size = (1024, 1024)
    trans = transforms.Compose([
        transforms.Resize(input_size),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) # imagenet
    ])
    trans_label = transforms.Compose([
        transforms.Resize(input_size),
        transforms.ToTensor(),
    ])
    train_set = dataset.CropSegmentationDataset("train", transform = trans, target_transform=trans_label)
    val_set = dataset.CropSegmentationDataset("val", transform = trans, target_transform=trans_label)
    image_datasets = {
        'train': train_set, 'val': val_set
    }
    dataloaders = {
        'train': DataLoader(train_set, batch_size=batchsize, shuffle=True, num_workers=0),
        'val': DataLoader(val_set, batch_size=batchsize, shuffle=True, num_workers=0)
    }
    if(weights==True):
        class_weight_loader = DataLoader(train_set, batch_size=1, shuffle=True, num_workers=0)
        weights_sum = torch.zeros(3)
        for inputs, labels in class_weight_loader:
            weights = weight_class(labels)
            weights_sum += weights
        mean_weights = weights_sum / len(class_weight_loader)
        return dataloaders, mean_weights
    return dataloaders
def print_metrics(metrics, epoch_samples, phase):
    outputs = []
    for k in metrics.keys():
        outputs.append("{}: {:4f}".format(k, metrics[k] / epoch_samples))

    print("{}: {}".format(phase, ", ".join(outputs)))
def get_loss(outputs,labels,metrics,weight = None):
    """
    :param outputs:batch of preds
    :param labels:batch of labels
    :param metrics: to track loss
    :param weight: Only used when using class weights
    :return: Loss
    """

    """If you want to train with weights """
    loss = F.cross_entropy(outputs,labels.long())#,weight=weights)
    """Used for Focal loss. Then uncomment that part"""
    # labels_oneHot =convert_target_onehot_batch(labels).to(device)
    # loss = torchvision.ops.sigmoid_focal_loss(outputs,labels_oneHot)
    #
    #
    # # loss = F.cross_entropy(outputs, labels.long())
    # # Sum the loss over all elements in the batch
    # loss_sum = torch.sum(loss)

    # Update metrics
    metrics['loss'] += loss.item()

    # Compute the mean loss per sample
    #batch_size = labels.size(0)
    return loss #/ batch_size
def train_model(model, optimizer, scheduler, num_epochs,batch_size):
    """
    Used to train model
    """
    """Uncomment the line if you want to train with class weights for the CrossEntropy"""
    #dataloaders,weights = get_data_loaders(batch_size,weights=True)
    dataloaders = get_data_loaders(batch_size)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    best_model_wts = copy.deepcopy(model.state_dict())
    best_loss = 1e10
    train_losses = []  # to store training losses
    val_losses = []  # to store validation losses

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        since = time.time()

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                for param_group in optimizer.param_groups:
                    print("LR", param_group['lr'])

                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            metrics = defaultdict(float)
            epoch_samples = 0
            #data_loader = tqdm(dataloaders[phase], desc=f'{phase} Epoch {epoch}/{num_epochs - 1}', dynamic_ncols=True)
            data_loader = tqdm(dataloaders[phase], desc='{} Epoch {}/{}'.format(phase, epoch, num_epochs - 1),
                               dynamic_ncols=True)

            for inputs, labels in data_loader :
                inputs = inputs.to(device).float()
                labels = labels.to(device).float()
                #weights = weights.to(device).float()

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs).float()

                    loss = get_loss(outputs,labels,metrics,device)
                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                    correct = 0
                    # if phase== "val":
                    #     correct += (outputs == labels).float().sum()
                    #     accuracy = 100 * correct / len(batch_size)
                # if phase == 'train':
                #     scheduler.step()

                # statistics
                epoch_samples += inputs.size(0)

            print_metrics(metrics, epoch_samples, phase)
            epoch_loss = metrics['loss'] / epoch_samples
            if phase == 'train':
                train_losses.append(epoch_loss)
            else:
                val_losses.append(epoch_loss)
            # deep copy the model
            if phase == 'val' and epoch_loss < best_loss:
                print("saving best model")
                best_loss = epoch_loss
                best_model_wts = copy.deepcopy(model.state_dict())

        time_elapsed = time.time() - since
        print('{:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    print('Best val loss: {:4f}'.format(best_loss))

    # load best model weights
    model.load_state_dict(best_model_wts)
    save_path = 'Unet_in_full_HD.pth'
    torch.save(model.state_dict(), save_path)
    plt.plot(train_losses, label='Train Loss')
    plt.plot(val_losses, label='Validation Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig('loss_plot.png')

    return model
def run(UNet,epoch,batchsize):
    num_class = 3
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)
    model = UNet(num_class).to(device)
    optimizer_ft = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=1e-4)

    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=30, gamma=0.1)

    model = train_model(model, optimizer_ft, exp_lr_scheduler, num_epochs=epoch,batch_size=batchsize)
def visualization(inputs,pred,labels):
    """
    Visualize the first prediction in the batch.
    :param inputs: Batch Images which are given the model
    :param pred: prediction from model already with softmax and one hot encoded.
    Use:
    #pred = model(inputs)
    #pred = torch.softmax(pred,dim=1)
    #pred = get_arg_class(pred,0).cpu()

    :param labels: batch of labels
    :return: None
    """

    pred_withclassid = get_original_label_from_onehot(pred)

    # Unnormalize and convert to numpy array for visualization
    image_tensor = torch.clamp(inputs[0], 0, 1)
    image_np = (image_tensor.permute(1, 2, 0).numpy() * 255).astype(np.uint8)
    label_np = labels[0].numpy()

    """Visualize image and label with original in 0,1,2"""
    # Plot the image and labels side by side with colorbars
    fig, axs = plt.subplots(1, 3, figsize=(12, 6))

    im0 = axs[0].imshow(image_np)
    axs[0].set_title('Original Image')
    cbar0 = plt.colorbar(im0, ax=axs[0], fraction=0.046, pad=0.04)

    im1 = axs[1].imshow(label_np, cmap='viridis', vmin=0, vmax=2)  # Assuming 0-4 are the class labels
    axs[1].set_title('Segmentation Label')
    cbar1 = plt.colorbar(im1, ax=axs[1], ticks=[0, 1, 2], fraction=0.046, pad=0.04)
    cbar1.set_ticklabels(['background', 'crop', 'weed'])

    im2 = axs[2].imshow(pred_withclassid, cmap='viridis', vmin=0, vmax=2)  # Assuming 0-4 are the class labels
    axs[2].set_title('Segmentation Predicted')
    cbar1 = plt.colorbar(im2, ax=axs[2], ticks=[0, 1, 2], fraction=0.046, pad=0.04)
    cbar1.set_ticklabels(['background', 'crop', 'weed'])

    plt.tight_layout()
    plt.show()

    """visualize the per class segmentation"""
    label_one_hot = convert_target_onehot(labels[0, :, :])
    print("one_hot: ", label_one_hot.shape)

    pred = pred.data.numpy()
    fig, axs = plt.subplots(2, 3, figsize=(12, 6))

    label_channel = label_one_hot[0, :, :]  # Extract the 2D array from the first channel
    im0 = axs[0, 0].imshow(label_channel, cmap='viridis')
    axs[0, 0].set_title('Ground')
    colorbar0 = fig.colorbar(im0, ax=axs[0, 0], fraction=0.046, pad=0.04)

    label_channel = label_one_hot[1, :, :]  # Extract the 2D array from the first channel
    im1 = axs[0, 1].imshow(label_channel, cmap='viridis')
    axs[0, 1].set_title('crop')
    colorbar1 = fig.colorbar(im1, ax=axs[0, 1], fraction=0.046, pad=0.04)

    label_channel = label_one_hot[2, :, :]  # Extract the 2D array from the first channel
    im2 = axs[0, 2].imshow(label_channel, cmap='viridis')
    axs[0, 2].set_title('weed')
    colorbar2 = fig.colorbar(im2, ax=axs[0, 2], fraction=0.046, pad=0.04)

    image_np = pred[0, :, :]
    im3 = axs[1, 0].imshow(image_np, cmap='viridis')  # specify cmap to match labels
    axs[1, 0].set_title('Prediction background')
    colorbar3 = fig.colorbar(im3, ax=axs[1, 0], fraction=0.046, pad=0.04)

    image_np = pred[1, :, :]
    im4 = axs[1, 1].imshow(image_np, cmap='viridis')  # specify cmap to match labels
    axs[1, 1].set_title('Prediction crop')
    colorbar4 = fig.colorbar(im4, ax=axs[1, 1], fraction=0.046, pad=0.04)

    image_np = pred[2, :, :]
    im5 = axs[1, 2].imshow(image_np, cmap='viridis')  # specify cmap to match labels
    axs[1, 2].set_title('Prediction weed')
    colorbar5 = fig.colorbar(im5, ax=axs[1, 2], fraction=0.046, pad=0.04)

    plt.tight_layout(pad=2.0)
    plt.show()
def get_original_label_from_onehot(pred):
    """get from one hot and three channel -> one channel and classes 0,1,2"""
    result_mask = np.zeros_like(pred[0])
    result_mask[pred[0] == 1] = 0
    result_mask[pred[1] == 1] = 1
    result_mask[pred[2] == 1] = 2
    return result_mask
def get_arg_class(pred, batch):
    """
    One hot encodes the Prediction
    Use torch.argmax to find the index of the maximum value along the channel dimension.
    Sets the other two channels to zero and the channel with max value to one
    :param pred:
    :param batch:
    :return:
    """
    argmax_result = torch.argmax(pred[batch], dim=0, keepdim=True)
    one_hot_encoding = torch.zeros_like(pred[batch])
    one_hot_encoding.scatter_(0, argmax_result, 1)
    return one_hot_encoding
def convert_target_onehot(target):
    """
    converts the one channel label  to three channel one hot enconded label
    :param targets: label with the labels 0,1,2 for the three classes
    :return: label with three channels where every channel represents the class.
    First channel -> for class 0
    Second channel -> for class 1
    Third channel -> for class 2
    """
    target_one_hot = torch.zeros((3, target.shape[0], target.shape[1]), dtype=torch.float32)
    for class_idx in range(3):
        target_one_hot[class_idx, :, :] = (target[ :, :] == class_idx).float()
    return target_one_hot
def convert_target_onehot_batch(targets):
    """
    converts the one channel label batch to three channel one hot enconded label batch
    :param targets: label with the labels 0,1,2 for the three classes
    :return: label with three channels where every channel represents the class.
    First channel -> for class 0
    Second channel -> for class 1
    Third channel -> for class 2
    """
    """This function is only used for the Focal loss"""
    batch_size, height, width = targets.shape
    target_one_hot = torch.zeros((batch_size, 3, height, width), dtype=torch.float32)
    for class_idx in range(3):
        target_one_hot[:, class_idx, :, :] = (targets[:, :, :] == class_idx).float()

    return target_one_hot
def intersection_over_union(output, label,device, num_classes=3):
    """
    Helper for IoUcalculation. Calculates the IoU for a single image.
    """
    # Convert the tensors to Long data type for indexing
    output = output.to(device)
    label = label.long().to(device)
    # Initialize variables to store intersection and union areas for each class
    intersection = torch.zeros(num_classes)
    union = torch.zeros(num_classes)

    # Calculate intersection and union for each class
    for class_idx in range(num_classes):
        intersection[class_idx] = torch.sum((output[class_idx] == 1) & (label[class_idx] == 1))
        union[class_idx] = torch.sum((output[class_idx] == 1) | (label[class_idx] == 1))

        # Check for division by zero
        if union[class_idx] == 0:
            #To get rid of zero division. Because somethimes there are no weeds in a image.
            union[class_idx] = 0.000000000000000000000000000000000000000000001

    # Calculate IoU for each class
    iou_per_class = intersection / union
    # Calculate mean IoU across all classes
    mean_iou = torch.mean(iou_per_class)
    return iou_per_class, mean_iou
def calculate_iou_from_loader(model, dataloader, device, num_classes=3):
    """
    calculates the Intersection over Union for the hole dataloader.
    Use the validationset
    :param model: model_to_use
    :param dataloader: valaidation dataloader
    :param device: cuda or cpu
    :param num_classes: 3
    :return: IoU
    """
    model.eval()
    iou_per_class_total = torch.zeros(3)
    mean_total = 0

    with torch.no_grad():
        for inputs, labels in dataloader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)  # Assuming model returns one-hot encoded tensors
            pred = torch.softmax(outputs, dim=1)
            batch = 2

            for i in range(batch):
                label_one_hot = convert_target_onehot(labels[i, :, :])
                pred = get_arg_class(pred, i)
                iou_per_class, mean = intersection_over_union(pred, label_one_hot, device, num_classes)
                iou_per_class_total += iou_per_class
                mean_total += mean
            #print(iou_per_class_total,mean_total)

    # Calculate IoU for each class
    iou_per_class_total = iou_per_class_total / len(dataloader.dataset)
    mean_total = mean_total / len(dataloader.dataset)
    print(len(dataloader.dataset),"lendata")
    # Calculate mean IoU across all classes
    return iou_per_class_total, mean_total
def interference(UNet):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = UNet(3).to(device)
    model.load_state_dict(torch.load("Unet_in_full_HD.pth"))
    model.eval()  # Set model to the evaluation mode
    dataloaders = get_data_loaders(2,weights=False)
    test_loader= dataloaders["val"]
    """Comment in for Intersection over Union"""
    # num_classes = 3  # Adjust based on the number of classes in your segmentation task
    #
    # iou_per_class, mean_iou = calculate_iou_from_loader(model, test_loader, device, num_classes)
    #
    # print("IoU per class:", iou_per_class)
    # print("Mean IoU:", mean_iou.item())

    """Visualizing and plot"""
    # Get the first batch
    inputs, labels = next(iter(test_loader))
    print("label: ",labels.shape)
    inputs = inputs.to(device)
    labels = labels
    print("input: ",inputs.shape)

    # Predict
    pred = model(inputs)
    """after the model a softmax and the one hot encoding has to be applied"""
    pred = torch.softmax(pred,dim=1)
    pred = get_arg_class(pred,0).cpu()
    print("pred: ",pred.shape)
    # pred_squeezed = get_original_label_from_onehot(pred)
    inputs = inputs.cpu()
    labels = labels.cpu()
    visualization(inputs,pred,labels)

