from scipy.signal import wiener
import numpy as np
from PIL import Image

class WienerFilterDenoiser:
    @staticmethod
    def denoise(image):
        # Convert PIL.Image to NumPy array
        image_np = np.array(image)

        # Assuming the image_np is an RGB image
        denoised_channels = [wiener(channel, mysize=3, noise=0.1) for channel in
                             np.split(image_np, image_np.shape[-1], axis=-1)]
        denoised_image_np = np.concatenate(denoised_channels, axis=-1)

        # Ensure the denoised_image_np is in the valid range for an image
        denoised_image_np = np.clip(denoised_image_np, 0, 255).astype(np.uint8)

        # Convert NumPy array back to PIL.Image
        denoised_image = Image.fromarray(denoised_image_np)
        return denoised_image