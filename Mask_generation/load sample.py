import matplotlib.pyplot as plt
import torch
from diffusers import DDPMPipeline


def get_event():
    from tensorboard.backend.event_processing import event_accumulator

    # Provide the path to your events file
    events_file_path = 'outputmask256x256_normalized/logs/train_example/events.out.tfevents.1702985548.Bumblebee.21960.0'

    # Create an EventAccumulator
    event_acc = event_accumulator.EventAccumulator(events_file_path)
    event_acc.Reload()

    # Get the available scalar tags
    scalar_tags = event_acc.Tags()['scalars']

    # Access scalar values for a specific tag (e.g., 'loss')
    loss_values = event_acc.Scalars('loss')

    # Extract epoch and loss values for plotting
    epochs = [event.step for event in loss_values]  # Assuming steps represent epochs
    losses = [event.value for event in loss_values]

    # Plot the loss values over epochs
    plt.plot(epochs, losses, label='Loss')
    plt.title('Loss Over Epochs')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()


def interference():
    # Set the device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load the DDPMPipeline
    pipe = DDPMPipeline.from_pretrained("outputmask256x256_normalized").to(device)


    num_images = 3
    fig, axs = plt.subplots(1, 3, figsize=(12, 6))

    # Loop through the images and plot them on the grid
    for i in range(num_images):
        images = pipe().images[0]
        im = axs[i].imshow(images)
        axs[i].set_title(f'Segmentation Label {i + 1}')
        cbar = plt.colorbar(im, ax=axs[i])
    # Adjust layout
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    #get_event()
    interference()
